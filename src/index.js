import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import './styles/index.css';
import Masthead from "./layouts/Masthead";
import SiteFooter from "./layouts/SiteFooter";
import DealerSearch from "./layouts/DealerSearch";

class App extends Component {
    render() {
        return (
            <div id={'app'} className={'app'}>
                <Masthead/>
                <div id={'content'}>
                    <div className={'banner'}></div>
                    <DealerSearch/>
                </div>
                <SiteFooter/>
            </div>
        );
    }
}

ReactDOM.render(<App />, document.getElementById('root'));
