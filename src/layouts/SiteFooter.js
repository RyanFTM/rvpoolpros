import React from 'react';
import footerLogo from '../assets/images/pool-pros-logo-footer.png';
import twitterIcon from '../assets/images/twitter-icon.png';
import facebookIcon from '../assets/images/facebook-icon.png';
import youtubeIcon from '../assets/images/youtube-icon.png';

const SiteFooter = (props) => {
    return(
        <div className={'site-footer'}>
            <div className={'content'}>
                <div className={'container'}>
                    <div className={'logo-wrapper'}>
                        <img src={footerLogo} alt="Pool Pros Footer Logo"/>
                    </div>
                    <div className={'socials'}>
                        <span>Connect with Us</span>
                        <ul>
                            <li>
                                <a href="##" className={'social-icon'}>
                                    <img src={twitterIcon} alt="Pool Pros Twitter Icon"/>
                                </a>
                            </li>
                            <li>
                                <a href="##" className={'social-icon'}>
                                    <img src={facebookIcon} alt="Pool Pros Facebook Icon"/>
                                </a>
                            </li>
                            <li>
                                <a href="##" className={'social-icon'}>
                                    <img src={youtubeIcon} alt="Pool Pros Youtube Icon"/>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div className={'meta'}>
                <div className={'container'}>
                    <ul>
                        <li>
                            &copy; 2018 Pool Pros
                        </li>
                        <li>
                            <a href="##">Privacy Policy</a>
                        </li>
                        <li>
                            <a href="##">Terms and Conditions</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    )
}

export default SiteFooter;
