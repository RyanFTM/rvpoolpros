import React, {Component} from 'react';
import DealerCard from "../components/DealerCard";
import Filter from "../components/Filter";

const dealersData = require('../api/dealers.json');

export default class DealerSearch extends Component {
    state = {
        data: dealersData,
        dealers: [],
        filters: [
            {
                name: 'Service',
                checked: true,
                tooltip: false,
            },
            {
                name: 'Installation',
                checked: true,
                tooltip: false
            },
            {
                name: 'Residential',
                checked: true,
                tooltip: false
            },
            {
                name: 'Commercial',
                checked: false,
                tooltip: true
            },
        ]
    }

    // TODO Add function to simulate loading from an API request instead of a json file

    setFilter = (data) =>{
        let filter = this.state.filters.filter(filter => filter.name === data.name )[0]
        console.log(filter);
        // TODO Apply appropriate filters to dealers array
    }

    componentDidMount() {
        this.setState({
            dealers: dealersData.dealers
        })
    }

    render() {
        const {data, dealers, filters} = this.state;
        return (
            <div className={'dealer-search container padded'}>
                <div className={'dealer-filters'}>
                    <div className={'container flex centered flex-between'}>
                        <div className={'results'}>
                            <span className={'amount'}>{dealers.length} dealers in {data.zipcode}</span>
                        </div>
                        <div className={'filters-list'}>
                            <header>Filter Results</header>
                            <ul className={'filters'}>
                                {filters.map((filter, index) => {
                                    return (
                                        <Filter {...filter} key={index} setFilter={this.setFilter}/>
                                    )
                                })}
                            </ul>
                        </div>
                    </div>
                </div>
                <div className={'dealer-grid'}>
                    {dealers.map((dealer, index) => {
                        return <DealerCard data={dealer.data} key={`${index}-${dealer.data.companyID}`}/>
                    })}
                </div>
            </div>
        )
    }
}
