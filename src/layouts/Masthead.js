import React from 'react';
import logo from '../assets/images/pool-pros-logo.png';

const Masthead = (props) =>{
    return (
        <div className={'masthead'}>
            <div className={'meta'}>
                <div className={'container flex centered flex-end'}>
                    <a href="##">Dealers and Distributors</a>
                    <a href="##">Commercial Service <i className={'ss-action'}></i></a>
                </div>
            </div>
            <div className={'navigation'}>
                <div className={'container flex centered flex-between'}>
                    <div className={'logo-wrapper'}>
                        <img src={logo} alt="Pool Pros logo"/>
                    </div>
                    {/* TODO Mobile Menu Responsive etc*/}
                    <nav className={'main-nav'}>
                        <ul>
                            <li>
                                <a href="##">Pools &amp; Spas</a>
                            </li>
                            <li>
                                <a href="##">Supplies</a>
                            </li>
                            <li>
                                <a href="##">Resources</a>
                            </li>
                            <li>
                                <a href="##">Services</a>
                            </li>
                            <li className={'callout'}>
                                <a href="##"><i className={'ss-location'}></i> Find a Pool Pro</a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    )
}

export default Masthead;
