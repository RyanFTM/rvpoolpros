import React, {Component} from 'react';

export default class DealerCard extends Component {

    formatPhone = (phone, replacement = '') => {
        return phone.replace(/[^A-Za-z0-9]/g, replacement);
    }

    isClosed = (hours) => {
        switch (hours){
            case 'On Call':
                return `- ${hours}`
            case '':
                return '- CLOSED'
            default:
                return hours
        }
    }

    determineCertification = (certification) => {
        switch (certification){
            case 'Installation Pro':
                return 'ss-star'
            case 'Service Pro':
                return 'ss-settings'
            case 'Residential Pro':
                return 'ss-home'
            case 'Commercial Pro':
                return 'ss-users'
            default:
                return 'ss-star'
        }
    }

    render() {
        const {data} = this.props;
        return (
            <div className={'dealer-card'}>
                <div className={'content'}>
                    <header className={'name'}>
                        {data.name}
                    </header>
                    <main>
                        <span className={'phone'}>
                            <a href={`tel:${this.formatPhone(data.phone1)}`}>
                                <i className={'phone-icon'}></i>
                                {this.formatPhone(data.phone1, '.')}</a>
                        </span>
                        <p><i>Can't talk now? Click below to send an email.</i></p>
                        <a className={'mail-pro'} href={"##"}><i className={'ss-mail'}></i>Contact This Pro</a>
                        <div className={'hours'}>
                            <header>Business Hours</header>
                            <p>Weekdays {this.isClosed(data.weekHours.mon)}</p>
                            <p>Saturdays {this.isClosed(data.weekHours.sat)}</p>
                            <p>Sundays {this.isClosed(data.weekHours.sun)}</p>
                        </div>
                    </main>
                    <div className={'certifications'}>
                        {data.certifications.map((certification, index) => {
                            return(
                                <div className={'certification'} key={index}>
                                    <i className={this.determineCertification(certification)}></i>
                                    {certification}</div>
                            )
                        })}
                    </div>
                </div>
            </div>
        )
    }
}
