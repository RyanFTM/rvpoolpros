import React from 'react';

const Filter = (props) => {
    return(
        <li className={'filter'}>
            <input type="checkbox" checked={props.checked} onChange={() => props.setFilter(props)} value={props.name}/>
            <label>{props.name}</label>
        </li>
    )
}

export default Filter;
